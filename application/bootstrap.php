<?php
@session_start();
// Include all files of core
require_once APPLICATION_PATH.'/configs/config.php';
require_once APPLICATION_PATH.'/classes/db.php';
require_once APPLICATION_PATH.'/core/model.php';
require_once APPLICATION_PATH.'/core/view.php';
require_once APPLICATION_PATH.'/core/controller.php';
require_once APPLICATION_PATH.'/core/route.php';
require_once APPLICATION_PATH.'/lib/PHPExcel.php';
Model::$db = db::getInstance();
// Run router
Route::run();

