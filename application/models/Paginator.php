<?php
class Paginator
{
    public $pageCount;
    public $current;
    public $previous;
    public $next;
    public $pagesInRange;

    public $page = 0;


    public function  __construct($cnt_all, $cntPerPage, $current, $cnt_links = 6)
    {
        $cc = $cnt_all > $cntPerPage ? ($cnt_all % $cntPerPage == 0 ? $cnt_all / $cntPerPage : 1 + (int)($cnt_all / $cntPerPage)) : 1;
        if ($current > 0 && $current <= $cc) {
            $this->page = $current-1;
        }

        $this->pageCount = $cc;
        if ($current>1){
            $this->previous = $current-1;
        }
        if ($current<$cc){
            $this->next = $current+1;
        }
        $this->pagesInRange = array();

        for ($i = $current-($cnt_links/2);$i<$current+(1+$cnt_links/2);++$i){

            if ($i>0&&$i<=$cc)
                $this->pagesInRange[] = (int)$i;
        }
        $this->current = $current;


        return $this;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function renderLinks()
    {
        ?>
        <ul class="pagination">
        <?php if ($this->pageCount>1): ?>

        <?php if (isset($this->previous)):?>
            <li><a href="?p=<?=$this->previous?>">
                    <
                </a></li>
        <?php endif; ?>

        <!-- links -->
        <?php foreach ($this->pagesInRange as $page): ?>
            <?php if ($page != $this->current): ?>
                <li><a href="?p=<?=$page?>">
                        <?php echo $page; ?>
                    </a> </li>
            <?php else: ?>
                <li class="active" >
                    <a href="javascript:void(0);"> <?php echo $page; ?></a>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>

        <!-- link to next page -->
        <?php if (isset($this->next)):?>
            <li><a href="?p=<?=$this->next?>">
                    >
                </a></li>
        <?php endif;  ?>

    <?php endif; ?>
        </ul><?php
    }

}