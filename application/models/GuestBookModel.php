<?php

/**
 * Class GuestBookModel
 */
class GuestBookModel extends Model
{
    /**
     * Inserts new post to Guest Book
     * @param $data
     * @return int - id of inserted entries
     */
    public function addNew($data)
    {
        self::$db->query('INSERT INTO `tb_guest_book` (`id`, `username`, `email`, `site`, `text`, `ip_address`, `web_agent`, `date`) VALUES (NULL, ?s, ?s, ?s, ?s, ?s, ?s, CURRENT_TIMESTAMP);', $data['name'], $data['email'], $data['site'], $data['text']
            , $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']);

        return self::$db->last_insert_id();
    }

    /**
     * Count of entries in table
     * @return array
     */
    public function getCount()
    {
        $res = self::$db->getOne('SELECT count(*) FROM tb_guest_book');
        return $res;
    }

    /**
     * Gets entries with limit and offset
     * @param $limit - start position
     * @param $offset - count of elements
     * @return array
     */
    public function _getAll($limit, $offset)
    {
        $query = 'SELECT * FROM tb_guest_book ';
        if (isset($_SESSION['sort']['table'])
            && isset($_SESSION['sort']['field'])
            && isset($_SESSION['sort']['order'])
            && $_SESSION['sort']['table'] == 'gb'
        ) {
            $query .= 'ORDER BY ' . $_SESSION['sort']['field'] . ' ' . $_SESSION['sort']['order'];
        }
        $query .= ' LIMIT ?i,?i';
        return self::$db->getAll($query, $offset, $limit);
    }

    /**
     * Gets all entries from DB
     * @return array
     */
    public function getAll()
    {
        $query = 'SELECT * FROM tb_guest_book ';
        return self::$db->getAll($query);
    }

    /**
     * Removes item by id
     * @param $id
     */
    public function _delete($id)
    {
        self::$db->query('DELETE FROM tb_guest_book WHERE id=?i', $id);
    }

}
