<section>
    <h1>Guest Book</h1>

    <div class="slide-panel">
        <span class="gb-add" onclick="newFeedback();">Add feedback</span>
        <div> <a href ="/guestbook/export"><span class="export"></span> EXPORT</a></div>
    </div>

    <table id="gb-table-all" class="table table-striped table-bordered">
        <thead>
        <tr class="sortable">
            <th data-sort="id">
                ID<span <?php echo ($_SESSION['sort']['field'] == 'id') && $_SESSION['sort']['order'] == 'DESC' ? 'class="rotate"' : '' ?>></span>
            </th>
            <th data-sort="username">
                Name<span <?php echo ($_SESSION['sort']['field'] == 'username') && $_SESSION['sort']['order'] == 'DESC' ? 'class="rotate"' : '' ?>></span>
            </th>
            <th data-sort="email">
                Email<span <?php echo ($_SESSION['sort']['field'] == 'email') && $_SESSION['sort']['order'] == 'DESC' ? 'class="rotate"' : '' ?>></span>
            </th>
            <th data-sort="date">
                Date<span <?php echo ($_SESSION['sort']['field'] == 'date') && $_SESSION['sort']['order'] == 'DESC' ? 'class="rotate"' : '' ?>></span>
            </th>
            <th data-sort="ip_address">
                IP<span <?php echo ($_SESSION['sort']['field'] == 'ip_address') && $_SESSION['sort']['order'] == 'DESC' ? 'class="rotate"' : '' ?>></span>
            </th>
            <th data-sort="site">Web
                site<span <?php echo ($_SESSION['sort']['field'] == 'site') && $_SESSION['sort']['order'] == 'DESC' ? 'class="rotate"' : '' ?>></span>
            </th>
            <th data-sort="text">
                Text<span <?php echo ($_SESSION['sort']['field'] == 'text') && $_SESSION['sort']['order'] == 'DESC' ? 'class="rotate"' : '' ?>></span>
            </th>
        </tr>
        </thead>
        <tbody>

        <?php
        foreach ($data as $row) {
            ?>
            <tr>
                <td><?php echo $row['id']; ?></td>
                <td><?php echo $row['username']; ?></td>
                <td><?php echo $row['email']; ?></td>
                <td><?php echo $row['date']; ?></td>
                <td><?php echo $row['ip_address']; ?></td>
                <td><?php echo $row['site']; ?></td>
                <td><?php echo $row['text']; ?></td>
            </tr>
            <?php
        } ?>

        </tbody>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Date</th>
            <th>IP</th>
            <th>Web site</th>
            <th>Text</th>
        </tr>
        </tfoot>
    </table>
    <?php $paginator->renderLinks(); ?>


    <script>
        $().ready(function () {

            $('.close').click(function () {
                $('.modal').hide();
            });

            //callback handler for form submit
            $('#gb-add-post').submit(function (e) {
                var postData = $(this).serializeArray();
                var formURL = $(this).attr("action");
                $('span[id^=gb-msg-]').empty();

                $.post(formURL, postData, function (data) {
                    data = JSON.parse(data);
                    console.log(data['status']);
                    reloadCaptcha();
                    if (data['status'] == 'success') {
                        $("#gb-add-post")[0].reset();
                        showMsg(data['data']['message'], data['status']);
                    }
                    else {
                        console.log(data['data']);
                        $.each(data['data'], function (i, val) {
                            $("#gb-msg-" + i).html(val);
                        });
                    }
                });

                e.preventDefault(); //STOP default action
                //  e.unbind(); //unbind. to stop multiple form submit.
            });
        });

        function newFeedback() {
            $('#gb-new-post').show();
        }

    </script>

    <script>
        $('.sortable th').click(function () {

            var fields = $(this).data();
            if ($(this).find('span').hasClass('rotate')) {
                $(this).find('span').removeClass('rotate');
            }
            else {
                $(this).find('span').addClass('rotate');
            }

            var page = <?php echo $page;?>;
            $.post('/guestbook/filter', 'table=gb&field=' + fields['sort']+'&p='+page,
                function (data) {
                    //data: return data from server
                   var data = JSON.parse(data);
                    if(data['status'] == 'success') {
                        var str = '';
                        $.each(data['data'], function (i, val) {
                            str += '<tr>';

                                str += '<td>' + val['id'] + '</td>';
                                str += '<td>' + val['username'] + '</td>';
                                str += '<td>' + val['email'] + '</td>';
                                str += '<td>' + val['date'] + '</td>';
                                str += '<td>' + val['ip_address'] + '</td>';
                                str += '<td>' + val['site'] + '</td>';
                                str += '<td>' + val['text'] + '</td>';

                            str += '</tr>';
                        });
                        $('#gb-table-all > tbody').html(str);
                    }
                   // location.reload();
                }
            );
        });

        function reloadCaptcha() {
            $("#gb-img-captcha").attr("src", "captcha.php?" + (new Date()).getTime());
        }

    </script>

</section>

<!-- The Modal Success-->
<div id="gb-new-post" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">×</span>
            <h2>New feedback</h2>
        </div>
        <form id="gb-add-post" method="POST" action="/guestbook/addajax">
            <div class="modal-body">
                <p>
                <div>
                    <div>
                        <div><label for="gb-name">Name*</label></div>
                        <div>
                            <input type="text" name="gb-name" id="gb-name" placeholder="Name" required>
                        </div>
                        <span id="gb-msg-name" class="error"></span>
                    </div>

                    <div>
                        <div><label for="gb-email">Email</label></div>
                        <div>
                            <input type="text" name="gb-email" id="gb-email" placeholder="Email">
                        </div>
                        <span id="gb-msg-email" class="error"></span>
                    </div>

                    <div>
                        <div><label for="gb-site">Site*</label></div>
                        <div>
                            <input type="url" name="gb-site" id="gb-site" placeholder="Site" required>
                        </div>
                        <span id="gb-msg-site" class="error"></span>
                    </div>
                    <div>
                        <div><label for="gb-text">Text*</label></div>
                        <div>
                            <textarea name="gb-text" placeholder="Your text" rows="5" cols="25" required></textarea>
                        </div>
                        <span id="gb-msg-text" class="error"></span>
                    </div>
                    <div>
                        <div><label for="gb-captcha">Captcha*</label></div>
                        <div style="display: inline-flex; margin: 10px;"><img src="/captcha.php" id="gb-img-captcha"
                                                                              onclick="reloadCaptcha();">
                            <input type="text" name="gb-captcha" id="gb-captcha" placeholder="Captcha" class="captcha"
                                   size="20" required>
                        </div>
                        <span id="gb-msg-captcha" class="error"></span>
                    </div>
                </div>
                </p>
            </div>
            <div class="modal-footer">
                <input type="button" value="Cancel" onclick="$('.close').click();">
                <input type="submit" value="Submit">
            </div>
        </form>
    </div>
</div>

