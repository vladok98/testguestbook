<html lang="EN">
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content=""/>
<head>
    <!-- Include User stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <!-- jQuery -->
    <!--script src="//code.jquery.com/jquery-1.12.3.js"></script-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

    <!-- Noty -->
    <script src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
</head>
<title>Test Guest Book</title>
<body>
<header>
    <h1>Test Guest Book</h1>
</header>

<!-- Page Content -->
<div class="container">

    <nav>
        <ul>
            <li><a href="/guestbook">Guest Book </a></li>
            <li><a href="/admin"> Admin Panel</a></li>
        </ul>
    </nav>

    <?php include APPLICATION_PATH . '/views/' . $content_view; ?>
</div>
<footer>
    Copyright © Test Guest Book 2016
</footer>
<script type="text/javascript">

    function showMsg(msg,type) {
        var n = noty({
            text        : msg,
            type        : type,
            dismissQueue: true,
            layout      : 'top',
            theme       : 'defaultTheme',
            timeout		: 2000
        });
    }

</script>
</body>
</html>