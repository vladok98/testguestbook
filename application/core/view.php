<?php

class View
{

	/**
	 * @param $content_view - template for pages
	 * @param $template_view - template for content on page
	 * @param null $data - data from model
	 */
	function generate($content_view, $template_view, $data = null)
	{
		if($data !== null)
		{
			extract($data, EXTR_REFS);
		}
		include APPLICATION_PATH.'/views/'.$template_view;
	}

	function renderAjax($content_view,$data = null)
	{
		include APPLICATION_PATH.'/views/'.$content_view;
	}

}
