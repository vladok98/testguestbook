<?php
error_reporting(1);

class Route
{

    public static function run()
    {
        // Set default controller & action
        $defaultController = 'Index';
        $defaultAction = 'index';

        $urlRoutes = explode('/', $_SERVER['REQUEST_URI']);

        if(isset($urlRoutes[1])) {
            list($urlRoutes[1], $q) = explode('?', $urlRoutes[1], 2);
        }
        $ps = explode('&',$q);
        $params = array();
        foreach ($ps as $ss){
            list($k,$v) = explode('=',$ss,2);
            $params[$k]=$v;
        }

        // Get name of controller from request url
        $nameController = !empty($urlRoutes[1]) ? $urlRoutes[1] : $defaultController;
        // Get name of action from request url
        $nameAction = !empty($urlRoutes[2]) ? $urlRoutes[2] : $defaultAction;

        // Add suffixes to names of models, controllers, actions
        /*$nameModel = $nameController.'Model';*/
        $nameController .= 'Controller';
        $nameAction .= 'Action';

        /*// Include file of model(if exists)
        $fileModel = strtolower($nameModel) . '.php';
        $pathModel = APPLICATION_PATH . "/models/" . $fileModel;
        if(file_exists($pathModel))
        {
            include APPLICATION_PATH."/models/".$fileModel;
        }*/

        // Include file of controller
        $fileController = strtolower($nameController) . '.php';
        $pathController = APPLICATION_PATH . "/controllers/" . $fileController;
        if (file_exists($pathController)) {
            include APPLICATION_PATH . "/controllers/" . $fileController;
        } else {
            // If controller not found then redirect to 404 page
            self::error404();
        }

        // Creates controller object
        $controller = new $nameController;

        // Find action in this controller
        if (method_exists($controller, $nameAction)) {
            // Call action from controller
            $controller->$nameAction($params);
        } else {
            // If action not found then redirect to 404 page
            self::error404();
        }

    }

    public static function error404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';

        header('Location:' . $host . 'error/error404');
    }

}
