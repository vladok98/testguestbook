<?php

class FormValidator
{
    public static function isName($name, $required = true)
    {
        if (!$required && empty($name)) return true;
        return !empty($name) && preg_match("/^[a-zA-Z0-9 ]*$/",$name);
    }

    public static function isEmail($mail, $required = true)
    {
        if (!$required && empty($mail)) return true;
        if($required && empty($mail) && strlen($mail) > 255 && strpos($mail,'@') > 64) return false;
        return (!empty($mail) && preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',$mail));
    }

    public static function isURL($url, $required = true)
    {
        if (!$required && empty($url)) return true;
        return !empty($url) && preg_match('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i',$url);
    }

    public static function clearText($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}