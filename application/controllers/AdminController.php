<?php

/**
 * Class AdminController
 */
class AdminController extends Controller
{
    /**
     * AdminController constructor.
     */
    function __construct()
    {
        $this->model = new GuestBookModel();
        $this->view = new View();
    }

    /**
     * @param array $params
     */
    function indexAction($params = array())
    {
        $page = intval($params['p']);
        $cnt = $this->model->getCount();

        $perPage = 20;
        $pagin = new Paginator($cnt, $perPage, $page);

        $data = array();
        $data['data'] = $this->model->_getAll($perPage, $pagin->getPage() * $perPage);
        $data['paginator'] = $pagin;
        $data['page'] = $page == 0 ? 1 : $page;
        $this->view->generate('admin/index.php', 'template_view.php', $data);
    }

    /**
     * @param array $params
     */
    function removeajaxAction($params = array())
    {
        $id = intval($_POST['id']);
        $data = $this->model->_delete($id);
        $this->view->renderAjax('_helpers/json.php', array('status'=>'success', 'data'=>array('message' => 'Successfully deleted')));
    }

    /**
     * @param array $params
     */
    function filterAction($params = array())
    {
        $sort = in_array($_POST['field'], array('id','username','email', 'ip_address', 'site', 'web_agent', 'text','date')) ? $_POST['field'] : '';
        $order = $_SESSION['sort']['order'];
        $table = in_array($_POST['table'], array('gb'))? $_POST['table'] : 'gb';
        $_SESSION['sort']['table'] = $table;
        $_SESSION['sort']['field'] = $sort == "" ? $_SESSION['sort']['field'] = null ? 'id' : $_SESSION['sort']['field'] : $sort;
        $_SESSION['sort']['order'] = $order == null ? 'DESC' : ($order == 'DESC' ? 'ASC' : ($order == 'ASC' ? 'DESC' : 'ASC'));

        $page = intval($_POST['p']);
        $data = $this->model->_getAll(20,20*--$page);
        $this->view->renderAjax('_helpers/json.php', array('status'=>'success', 'data'=>$data,
            'sort'=>array('field'=>$_SESSION['sort']['field'],'order'=>$_SESSION['sort']['order'])));
    }

}
