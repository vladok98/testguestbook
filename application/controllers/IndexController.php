<?php

class IndexController extends Controller
{
	function __construct()
	{
		$this->view = new View();
	}
	function indexAction($params = array())
	{	
		$this->view->generate('index/index.php', 'template_view.php');
	}
}