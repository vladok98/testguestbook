<?php

/**
 * Class GuestBookController
 */
class GuestBookController extends Controller
{

	/**
	 * GuestBookController constructor.
	 */
	function __construct()
	{
		$this->model = new GuestBookModel();
		$this->view = new View();
	}

	/**
	 * @param array $params
	 */
	function indexAction($params = array())
	{

		$page = intval($params['p']);
		$cnt = $this->model->getCount();

		$perPage = 20;
		$pagin = new Paginator($cnt, $perPage, $page);

		$data = array();
		$data['data'] = $this->model->_getAll($perPage, $pagin->getPage() * $perPage);
		$data['paginator'] = $pagin;
		$data['page'] = $page == 0 ? 1 : $page;
		$this->view->generate('guestbook/index.php', 'template_view.php', $data);
	}

	/**
	 * @param array $params
	 */
	function addajaxAction($params = array())
	{

		$errors = array();
		$dataPOST = array();
		// Validate email
		if (!FormValidator::isEmail($_POST['gb-email'],false)) {

			$errors['email'] = "Invalid email format";
		}
		else{
			$dataPOST['email'] = strtolower($_POST['gb-email']);
		}
		// Validate name
		if (!FormValidator::isName($_POST['gb-name'])) {
			$errors['name'] = 'Field required. Only letters and digits.';
		}
		else{
			$dataPOST['name'] = $_POST['gb-name'];
		}
		// Validate URL
		if (!FormValidator::isURL($_POST['gb-site'])) {
			$errors['site'] = 'Invalid or empty URL';
		}
		else{
			$dataPOST['site'] = strtolower($_POST['gb-site']);
		}

		// Validate URL
		if (!FormValidator::clearText($_POST['gb-text'])) {
			$errors['text'] = 'Invalid or empty field';
		}
		else{
			$dataPOST['text'] = strtolower($_POST['gb-text']);
		}

		if ($_POST['gb-captcha'] != $_SESSION['secpic']) {
			$errors['captcha'] = 'Code from picture is not correct';
		}

		if(empty($errors)){
			$data = $this->model->addNew($dataPOST);
			$this->view->renderAjax('_helpers/json.php', array('status'=>'success', 'data'=>array('message' => 'Entries # ' . $data . ' Successfully added!')));
		}
		else{
			$this->view->renderAjax('_helpers/json.php', array('status'=>'error', 'data'=>$errors));
		}
	}

	/**
	 * Filter action
	 * @param array $params
	 */
	function filterAction($params = array())
	{
		$sort = in_array($_POST['field'], array('id','username','email', 'ip_address', 'site', 'web_agent', 'text', 'date')) ? $_POST['field'] : '';
		$order = $_SESSION['sort']['order'];
		$table = in_array($_POST['table'], array('gb'))? $_POST['table'] : 'gb';
		$_SESSION['sort']['table'] = $table;
		$_SESSION['sort']['field'] = $sort == "" ? $_SESSION['sort']['field'] = null ? 'id' : $_SESSION['sort']['field'] : $sort;
		$_SESSION['sort']['order'] = $order == null ? 'DESC' : ($order == 'DESC' ? 'ASC' : ($order == 'ASC' ? 'DESC' : 'ASC'));

		$page = intval($_POST['p']);
		$data = $this->model->_getAll(20,20*--$page);
		$this->view->renderAjax('_helpers/json.php', array('status'=>'success', 'data'=>$data,
			'sort'=>array('field'=>$_SESSION['sort']['field'],'order'=>$_SESSION['sort']['order'])));
	}

	/**
	 * Generates Excel document
	 * @param array $params
	 * @throws PHPExcel_Exception
	 */
	function exportAction($params = array())
	{
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("GB-export");
$objPHPExcel->getProperties()->setTitle("Office Document");
$objPHPExcel->getProperties()->setSubject("Office Document");
$objPHPExcel->getProperties()->setDescription("Posts export document.");


// Add some data
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Username');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'email');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'IP');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Agent');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Date');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Text');
$i = 1;
		foreach($this->model->getAll() as $data)
		{
			$i++;
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, $data['id']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $data['username']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $data['email']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $data['ip_address']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $data['web_agent']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $data['date']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, $data['text']);

		}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		@header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
		@header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		@header("Cache-Control: no-cache, must-revalidate");
		@header("Pragma: no-cache");
		@header("Content-type: application/vnd.ms-excel");
		@header("Content-Disposition: attachment; filename=export.xls");
$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
$objWriter->save('php://output');
		exit;
	}


}
