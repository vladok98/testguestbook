<?php
error_reporting(1);
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../www'));

// Autoloader for classes
function __autoload($name){

    $path = APPLICATION_PATH . "/controllers/" . $name . ".php";
    if (file_exists($path)) {
        include $path;
    }
    $path = APPLICATION_PATH . "/models/" . $name . ".php";
    if (file_exists($path)) {
        include $path;;
    }

    $path = APPLICATION_PATH . "/lib/PHPExcel/" . $name . ".php";
    if (file_exists($path)) {
        include $path;;
    }

    $path = APPLICATION_PATH . "/helpers/" . $name . ".php";
    if (file_exists($path)) {
        include $path;;
    }

}

require_once APPLICATION_PATH.'/bootstrap.php';
